namespace FizzBuzz.Test
{
    using FizzBuzz.Game;
    using Xunit;

    /// <summary>
    /// Test the Game Engines
    /// </summary>
    public class EngineTest
    {
        private readonly Engine _engine;
        private readonly Feature2Engine _engine2;

        public EngineTest()
        {
            _engine = new Engine();
            _engine2 = new Feature2Engine();
        }

        [Fact]
        public void Feature1BuzzValidTest()
        {
            Assert.Equal("buzz", _engine.Play(5));
            Assert.Equal("buzz", _engine.Play(20));
            Assert.Equal("buzz", _engine.Play(200));
        }

        [Fact]
        public void Feature1FizzBuzzInValidTest()
        {
            string gameResult = _engine.Play(1);
            Assert.Equal(1, 1);

            gameResult = _engine.Play(2);
            Assert.Equal(2, 2);

            gameResult = _engine.Play(4);
            Assert.Equal(4, 4);
        }

        [Fact]
        public void Feature1FizzBuzzValidTest()
        {
            Assert.Equal("fizz buzz", _engine.Play(15));
            Assert.Equal("fizz buzz", _engine.Play(45));
            Assert.Equal("fizz buzz", _engine.Play(315));
        }

        [Fact]
        public void Feature1FizzValidTest()
        {
            Assert.Equal("fizz", _engine.Play(3));
            Assert.Equal("fizz", _engine.Play(9));
            Assert.Equal("fizz", _engine.Play(123));
        }

        [Fact]
        public void Feature2BuzzPopValidTest()
        {
            Assert.Equal("buzz pop", _engine2.Play(35));
            Assert.Equal("buzz pop", _engine2.Play(70));
            Assert.Equal("buzz pop", _engine2.Play(140));
        }

        [Fact]
        public void Feature2FizzBuzzPopValidTest()
        {
            Assert.Equal("fizz buzz pop", _engine2.Play(105));
            Assert.Equal("fizz buzz pop", _engine2.Play(210));
            Assert.Equal("fizz buzz pop", _engine2.Play(315));
        }

        [Fact]
        public void Feature2FizzPopValidTest()
        {
            Assert.Equal("fizz pop", _engine2.Play(21));
            Assert.Equal("fizz pop", _engine2.Play(63));
            Assert.Equal("fizz pop", _engine2.Play(126));
        }

        [Fact]
        public void Feature2PopValidTest()
        {
            Assert.Equal("pop", _engine2.Play(7));
            Assert.Equal("pop", _engine2.Play(28));
            Assert.Equal("pop", _engine2.Play(77));
        }

        [Fact]
        public void Feature3InvalidValidReplaceFizzWordTest()
        {
            Feature2Engine newEngine2 = new Feature2Engine(2, 3, null);
            newEngine2.FizzWord = "fuzz";
            newEngine2.BuzzWord = "bizz";

            Assert.Equal("1", newEngine2.Play(1));
            Assert.Equal("fuzz", newEngine2.Play(2));
            Assert.Equal("fuzz", newEngine2.Play(8));

            Assert.Equal("fuzz", newEngine2.Play(4));
            Assert.Equal("bizz", newEngine2.Play(9));
            Assert.Equal("fuzz bizz", newEngine2.Play(12));
        }
    }
}