﻿/// <summary>
/// Feature 2 engine, will also take in account feature 3.
/// </summary>

namespace FizzBuzz.Game
{
    using System.Text;

    /// <summary>
    /// The feature 2 engine will allow the additional POP method.
    /// </summary>
    public class Feature2Engine : Engine
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="fizzMod">What mod value causes fizz</param>
        /// <param name="buzzMod">What mod value causes buzz</param>
        /// <param name="popMod">What mod value causes pop</param>
        public Feature2Engine(int? fizzMod, int? buzzMod, int? popMod) : base(fizzMod, buzzMod)
        {
            if (popMod.HasValue)
                this.PopMod = popMod.Value;
        }

        public Feature2Engine()
        {
        }

        /// <summary>
        /// The word that is used for pop.
        /// </summary>
        public string PopWord { get; set; } = "pop";

        /// <summary>
        /// The pop mod value.
        /// </summary>
        protected int PopMod { get; set; } = 7;

        /// <summary>
        /// Plays the game given the input.
        /// </summary>
        /// <param name="input">The value we are checking from the user.</param>
        /// <returns></returns>
        public override string Play(int input)
        {
            StringBuilder baseResult = new StringBuilder(base.Play(input));

            string tempVal = this.Pop(input);

            bool _validFizzBuzz = false;

            // no valid rule
            if (baseResult.ToString().Equals(input.ToString()))
            {
                _validFizzBuzz = false;
            }
            else
            {
                _validFizzBuzz = true;
            }

            // check the tempval to see if we have a valid pop
            if (tempVal.Equals(PopWord))
            {
                // just append
                if (_validFizzBuzz)
                {
                    baseResult.Append(" ");
                    baseResult.Append(tempVal);
                }
                // not valid fizzbuzz so the value is the input so we clear and append pop
                else
                {
                    baseResult.Clear();
                    baseResult.Append(tempVal);
                }
            }
            return baseResult.ToString().Trim();
        }

        /// <summary>
        /// The pop method.
        /// </summary>
        /// <param name="input">User input.</param>
        /// <returns></returns>
        public virtual string Pop(int input)
        {
            return this.Valid(input, this.PopMod) == true ? PopWord : input.ToString();
        }
    }
}