﻿/// <summary>
/// FizzBuzz game engine.
/// </summary>

namespace FizzBuzz.Game
{
    using System.Text;

    /// <summary>
    /// Default Engine of the fizz buzz game.
    /// </summary>
    public class Engine
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="fizzMod">The value to use for fizz mod.</param>
        /// <param name="buzzMod">The value to user for buzz mod.</param>
        public Engine(int? fizzMod, int? buzzMod)
        {
            if (fizzMod.HasValue)
                this.FizzMod = fizzMod.Value;
            if (buzzMod.HasValue)
                this.BuzzMod = buzzMod.Value;
        }

        public Engine()
        { }

        /// <summary>
        /// The string to use for valid buzz.
        /// </summary>
        public string BuzzWord { get; set; } = "buzz";

        /// <summary>
        /// The value we use for the fizz word.
        /// </summary>
        public string FizzWord { get; set; } = "fizz";

        /// <summary>
        /// The value for the buzz mod.
        /// </summary>
        protected int BuzzMod { get; set; } = 5;

        /// <summary>
        /// The valud for the fizz mod.
        /// </summary>
        protected int FizzMod { get; set; } = 3;

        /// <summary>
        /// The method that will check for valid Buzz.
        /// </summary>
        /// <param name="input">Number the user entered</param>
        /// <param name="modulator">The modulator</param>
        /// <returns></returns>
        public virtual string Buzz(int input)
        {
            return this.Valid(input, BuzzMod) == true ? BuzzWord : input.ToString();
        }

        /// <summary>
        /// The method that will check for valid Fizz.
        /// </summary>
        /// <param name="input">The number the user entered</param>
        /// <param name="modulator">The modulator</param>
        /// <returns></returns>
        public virtual string Fizz(int input)
        {
            return this.Valid(input, FizzMod) == true ? FizzWord : input.ToString();
        }

        /// <summary>
        /// Plays the game
        /// </summary>
        /// <param name="input">The value the user entered</param>
        /// <returns></returns>
        public virtual string Play(int input)
        {
            StringBuilder returnVal = new StringBuilder();
            string tempVal = string.Empty;
            tempVal = this.Fizz(input);

            // check for fizz
            if (tempVal.Equals(FizzWord))
            {
                returnVal.Append(tempVal);
                returnVal.Append(" ");
            }

            // clear the value
            tempVal = string.Empty;

            // check for buzz
            tempVal = this.Buzz(input);
            if (tempVal.Equals(BuzzWord))
            {
                returnVal.Append(tempVal);
                returnVal.Append(" ");
            }

            if (returnVal.Length > 0)
            {
                return returnVal.ToString().Trim();
            }
            else
            {
                returnVal.Append(input.ToString());
                return returnVal.ToString();
            }
        }

        /// <summary>
        /// Does the mod operation
        /// </summary>
        /// <param name="input">The value to check</param>
        /// <param name="modulator">The value to be used as the mod by</param>
        /// <returns></returns>
        protected bool Valid(int input, int modulator)
        {
            bool result = input % modulator == 0 ? true : false;

            return result;
        }
    }
}