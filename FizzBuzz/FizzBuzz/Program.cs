﻿using System;
using FizzBuzz.Game;

namespace FizzBuzz
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            while (0 == 0)
            {
                Console.WriteLine("Enter an integer: ");

                string input = Console.ReadLine();
                int valuePlayed = 0;
                if (int.TryParse(input, out valuePlayed))
                {
                    FizzBuzz.Game.Feature2Engine engine = new Feature2Engine(2, 3, null);
                    engine.FizzWord = "fuzz";
                    engine.BuzzWord = "bizz";
                    Console.WriteLine(engine.Play(valuePlayed));
                }
                else
                {
                    Console.WriteLine("Enter an integer value.");
                }
            }
        }
    }
}