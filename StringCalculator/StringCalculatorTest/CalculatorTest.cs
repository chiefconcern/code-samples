/// <summary>
/// StringCalculatorTest
/// </summary>

namespace StringCalculatorTest
{
    using System;
    using StringCalculator;
    using Xunit;

    /// <summary>
    /// Tests the Calculator
    /// </summary>
    public class CalculatorTest
    {
        /// <summary>
        /// Instance of calculator.
        /// </summary>
        private Calculator _calculator;

        /// <summary>
        /// ctor
        /// </summary>
        public CalculatorTest()
        {
            this._calculator = new Calculator();
        }

        /// <summary>
        /// Throws an exception if negative numbers passed.
        /// </summary>
        [Fact]
        public void CacluatorNegativeNumbers()
        {
            ArgumentException ex = Assert.Throws<ArgumentException>(() => _calculator.Add("//;\n1;-1"));

            Assert.Equal("negatives not allowed -1", ex.Message);
        }

        /// <summary>
        /// Make sure empty string returns 0.
        /// </summary>
        [Fact]
        public void CaculatorEmptyStringTest()
        {
            Assert.Equal(0, _calculator.Add(""));
        }

        /// <summary>
        /// Ignores values larger than 1000.
        /// </summary>
        [Fact]
        public void CalculatorAddValueLargerThan1000Ignored()
        {
            Assert.Equal(1, _calculator.Add("1,1001"));
        }

        /// <summary>
        /// Tests being able to add multiple custom delims.
        /// </summary>
        [Fact]
        public void CalculatorAddWithManyCustomDelims()
        {
            string stringTocheck = "//[***][&&&]\n1***2***3";
            Assert.Equal(6, _calculator.Add(stringTocheck));
        }

        /// <summary>
        /// Tests being able to use a single custom delim.
        /// </summary>
        [Fact]
        public void CalculatorAnyDelimTestSum()
        {
            Assert.Equal(3, _calculator.Add("//;\n1;2"));
        }

        /// <summary>
        /// Tests being able to add 2 numbers.
        /// </summary>
        [Fact]
        public void CalculatorTwoNumberTest()
        {
            Assert.Equal(3, _calculator.Add("1,2"));
        }
    }
}