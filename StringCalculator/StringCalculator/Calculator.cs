﻿/// <summary>
/// Calculator.
/// </summary>

namespace StringCalculator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Handles adding numbers in a string.
    /// </summary>
    public class Calculator
    {
        /// <summary>
        /// Adds the numbers.
        /// </summary>
        /// <param name="numbers">number string</param>
        /// <returns></returns>
        public int Add(string numbers)
        {
            if (numbers == string.Empty)
            {
                return 0;
            }

            List<string> charSeparators = GetStringDelimiters(ref numbers);

            string[] numberValues = ParseNumbers(numbers, charSeparators);

            ValidatNoNegatives(numberValues);

            return CalculateValue(numberValues);
        }

        /// <summary>
        /// Gets the delimiters
        /// </summary>
        /// <param name="numbers">The numberstring</param>
        /// <returns>The Delimiters</returns>
        public List<string> GetStringDelimiters(ref string numbers)
        {
            List<string> returnDelims = new List<string>(new string[] { ",", "\n" });

            if (numbers.StartsWith("//"))
            {
                int startOfNumbers = numbers.IndexOf('\n');
                string subDelims = numbers.Substring(2, startOfNumbers - 2);
                string subNumbers = numbers.Substring(startOfNumbers + 1, numbers.Length - startOfNumbers - 1);

                MatchCollection bracketMatches = Regex.Matches(subDelims, @"\[.*?\]");
                List<string> delims = new List<string>();
                if (bracketMatches.Count > 0)
                {
                    foreach (Match m in bracketMatches)
                    {
                        string delimiterMatch = m.Value.TrimStart('[').TrimEnd(']');
                        delims.Add(delimiterMatch);
                    }
                }
                else
                {
                    delims = subDelims.ToCharArray().Select(c => c.ToString()).ToList();
                }
                returnDelims.AddRange(delims);
                numbers = subNumbers;
            }

            return returnDelims;
        }

        /// <summary>
        /// Calculates the value
        /// </summary>
        /// <param name="numberValues">All the number values.</param>
        /// <returns>Total</returns>
        protected int CalculateValue(string[] numberValues)
        {
            int calculatedValue = 0;

            foreach (string s in numberValues)
            {
                int currentValue = int.Parse(s);

                if (currentValue <= 1000)
                {
                    calculatedValue += currentValue;
                }
            }
            return calculatedValue;
        }

        /// <summary>
        /// Parses the numbers from the string.
        /// </summary>
        /// <param name="numberString">Full number string with delims.</param>
        /// <param name="separators">Just the delimiters.</param>
        /// <returns></returns>
        protected string[] ParseNumbers(string numberString, List<string> separators)
        {
            string[] numberValues = numberString.Split(separators.ToArray(), StringSplitOptions.None);

            return numberValues;
        }

        /// <summary>
        /// Makes sure no negatives.
        /// </summary>
        /// <param name="value">Ints to check.</param>
        protected void ValidatNoNegatives(string[] value)
        {
            List<int> negatives = value.Where(n => int.Parse(n) < 0).Select(s => int.Parse(s)).ToList();

            if (negatives.Count > 0)
            {
                string negativeExceptionMsg = "negatives not allowed ";
                negativeExceptionMsg += string.Join(",", negatives);
                throw new ArgumentException(negativeExceptionMsg);
            }
        }
    }
}